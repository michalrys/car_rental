package com.carrental.carrental.settings;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class MvcConfiguration extends WebMvcConfigurationSupport {
    @Override
    protected void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        //FIXME do we need this ?
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler(
                        "/img/**",
                        "/imguser/**",
                        "/css/**",
                        "/js/**"
                )
                .addResourceLocations(
                        "classpath:/static/img/",
                        "classpath:/static/imguser/",
                        "classpath:/static/css/",
                        "classpath:/static/js/"
                        );
    }
}
