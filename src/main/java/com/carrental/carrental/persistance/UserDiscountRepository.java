package com.carrental.carrental.persistance;

import com.carrental.carrental.domain.UserDiscount;
import org.springframework.data.repository.CrudRepository;

public interface UserDiscountRepository extends CrudRepository<UserDiscount, Long> {
}
