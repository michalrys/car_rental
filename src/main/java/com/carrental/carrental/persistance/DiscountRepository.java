package com.carrental.carrental.persistance;

import com.carrental.carrental.domain.Discount;
import org.springframework.data.repository.CrudRepository;

public interface DiscountRepository extends CrudRepository<Discount, Long> {
    Discount findByName(String name);
}
