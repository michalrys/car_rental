package com.carrental.carrental.persistance;

import com.carrental.carrental.domain.CarBorrowing;
import com.carrental.carrental.domain.UserCar;
import org.springframework.data.repository.CrudRepository;

public interface CarBorrowingRepository extends CrudRepository<CarBorrowing,Long> {
    CarBorrowing findByUserCar(UserCar userCar);
}
