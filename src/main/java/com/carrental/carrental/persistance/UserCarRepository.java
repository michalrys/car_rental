package com.carrental.carrental.persistance;

import com.carrental.carrental.domain.Car;
import com.carrental.carrental.domain.User;
import com.carrental.carrental.domain.UserCar;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserCarRepository extends CrudRepository<UserCar, Long> {
    UserCar findByCar(Car car);
    List<UserCar> findByUser(User user);
}
