package com.carrental.carrental.persistance;

import com.carrental.carrental.domain.CarRepairing;
import org.springframework.data.repository.CrudRepository;

public interface CarRepairingRepository extends CrudRepository<CarRepairing, Long> {
}
