package com.carrental.carrental.tools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataValidator {
    private static Pattern pattern;
    private static Pattern pattern2;
    private static Matcher matcher;
    private static Matcher matcher2;

    public static boolean isWrongFirstName(String firstName) {
        if (firstName.isEmpty()) {
            return true;
        }
        pattern = Pattern.compile("^[A-ZŚŁŻŹĆ][a-ząęóśłżźćń]{2,}");
        matcher = pattern.matcher(firstName);

        return !matcher.matches();
    }

    public static String getFirstNameErrorMessage() {
        return "Given first name is wrong. " +
                "Must contain at least 3 characters and can not contain any other marks than letters.";
    }

    public static boolean isWrongLastName(String lastName) {
        if (lastName.isEmpty()) {
            return true;
        }
        pattern = Pattern.compile("^[A-ZŚŁŻŹĆ][a-ząęóśłżźćń]{2,}([-][A-ZŚŁŻŹĆ][a-ząęóśłżźćń]{2,})?");
        matcher = pattern.matcher(lastName);

        return !matcher.matches();
    }

    public static String getLastNameErrorMessage() {
        return "Given last name is wrong. It must contain at least 3 characters and can not contain " +
                "any other marks than letters. There is possibility to use two part last names - must be " +
                "seperated by \"-\", for example Nowak-Wesołowski.";
    }

    public static boolean isWrongLogin(String login) {
        if (login.isEmpty()) {
            return true;
        }
        pattern = Pattern.compile("[A-Za-z][A-Za-z0-9_]{2,}");
        matcher = pattern.matcher(login);

        return !matcher.matches();
    }

    public static String getLoginErrorMessage() {
        return "Given login is wrong. It must have letters a - z or A - Z or 0 - 9. " +
                "Minimal amount of characters is 3. Must start from letter.";
    }

    public static boolean isWrongPassword(String password) {
        if (password.isEmpty()) {
            return true;
        }
        pattern = Pattern.compile("[A-Za-z0-9_!@#$%]{5,}");
        matcher = pattern.matcher(password);

        pattern2 = Pattern.compile("[_!@#$%]");
        matcher2 = pattern2.matcher(password);

        return !matcher.matches() || !matcher2.find();
    }

    public static String getPasswordErrorMessage() {
        return "Given password is unacceptable. Password must have at least 5 characters from " +
        "a - z, A - Z, 0 - 9, !, @, #, $, %, _ and must contain at least one special character: ! or @ or " +
                "# or $ or % or _.";
    }

}
