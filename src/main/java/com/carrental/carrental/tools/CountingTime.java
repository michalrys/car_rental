package com.carrental.carrental.tools;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountingTime {

    private Integer countingTimeToChartMyWallet;

    public CountingTime() {
    }
}
