package com.carrental.carrental.tools;

public class DataConverter {
    public static String getStringWithFirstLetterCapitalized(String givenString) {
        if(givenString.isEmpty()) {
            return null;
        }
        return givenString.substring(0,1).toUpperCase() + givenString.substring(1).toLowerCase();
    }
}
