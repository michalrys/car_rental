package com.carrental.carrental.service;

import com.carrental.carrental.domain.Discount;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DiscountService {

    public List<Discount> getDiscounts() {
        List<Discount> discounts = new ArrayList<>();

        discounts.add(new Discount("+10% discount after 1st car",
                "User get additional discount for cars, starting from second rented car. " +
                        "Starting point is a number of rented cars.",
                0.10, 0.0, 2.0));

        discounts.add(new Discount("+5% discount after 3rd car",
                "User get additional discount for cars, starting from 3rd rented car. " +
                        "Starting point is a number of rented cars.",
                0.05, 0.0, 3.0));

        discounts.add(new Discount("+5% discount after 5th car",
                "User get additional discount for cars, starting from 5th rented car. " +
                        "Starting point is a number of rented cars.",
                0.05, 0.0, 5.0));

        discounts.add(new Discount("-50PLN discount after 1st car",
                "User get additional discount for cars, starting from second rented car. " +
                        "Starting point is a number of rented cars.",
                0.0, 50.0, 5.0));

        return discounts;
    }
}
