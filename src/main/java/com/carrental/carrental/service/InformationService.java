package com.carrental.carrental.service;

import com.carrental.carrental.domain.City;
import com.carrental.carrental.domain.Color;
import com.carrental.carrental.domain.Models;
import com.carrental.carrental.domain.State;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@Getter
@Setter
public class InformationService {

    private Long carId;  // TODO: its better to operate on car serial number not on id. Change it later.
    private Long walletId;

    private LocalDate enterDate;
    private LocalDate exitDate;
    private Double priceForDay;
    private Double priceForAllDay;

    private City cityEnter;
    private City cityExit;

    private Models model;
    private Color color;
    private State state;
    private Double price;
    private String urlPhoto;

    private Double cashInWallet;
}