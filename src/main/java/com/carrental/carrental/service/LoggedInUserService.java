package com.carrental.carrental.service;

import com.carrental.carrental.domain.SecurityUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class LoggedInUserService {
    public static String getLogin() {
        String result = "unknown";

        Object loginData = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (loginData instanceof UserDetails) {
            result = ((UserDetails) loginData).getUsername();
        } else {
            result = "Wrong login - sth went wrong.";
        }

        return result;
    }

    public static String getRole() {
        String result = "ROLE_USER";

        Object loginData = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (loginData instanceof UserDetails) {
            result = ((SecurityUserDetails) loginData).getRole();
        } else {
            result = "ROLE_USER";
        }

        return result;
    }
}