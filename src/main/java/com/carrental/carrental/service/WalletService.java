package com.carrental.carrental.service;

import com.carrental.carrental.domain.CarBorrowing;
import com.carrental.carrental.domain.CarRepairing;
import com.carrental.carrental.domain.Wallet;
import com.carrental.carrental.persistance.WalletRepository;
import com.carrental.carrental.tools.CountingTime;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Getter
@Setter
public class WalletService {

    @Autowired
    private WalletRepository walletRepository;

    private Wallet newWallet;


    public void addMoney (Double money, CarBorrowing carBorrowing){

        Double many = prepareNewWallet(money);
        newWallet = new Wallet();
        newWallet.setCashInWallet(many);
        newWallet.setTransactionDate(LocalDateTime.now());
        newWallet.setCarBorrowing(carBorrowing);
        walletRepository.save(newWallet);

    }

    public void addMoney (Double money, CarRepairing carRepairing){

        Double many = prepareNewWallet(money);
        newWallet = new Wallet();
        newWallet.setCashInWallet(many);
        newWallet.setTransactionDate(LocalDateTime.now());
        newWallet.setCarRepairing(carRepairing);

        walletRepository.save(newWallet);
    }

    public Double prepareNewWallet (Double money) {

        Iterable <Wallet> allWallet = walletRepository.findAll();

        List<Wallet> wallets = IteratorUtils.toList(allWallet.iterator());

        List<LocalDateTime> walletsDate = new ArrayList<>();

        for (Wallet wallet: wallets) {
            walletsDate.add(wallet.getTransactionDate());
        }

        Collections.sort(walletsDate, Collections.reverseOrder());

        LocalDateTime time = walletsDate.get(0);

        Wallet wallet = walletRepository.findWalletByTransactionDate(time);
        return wallet.addMoney(money);

    }

    public void cashFilter(Integer howManyDays, Model model){

        Iterable<Wallet> wallets = walletRepository.findAll();
        List<Wallet> wallet = new ArrayList<>();

        for (Wallet listWallet : wallets) {
            wallet.add(listWallet);
        }

        Collections.sort(wallet);

        if (howManyDays>wallet.size()){
            howManyDays = wallet.size();
        }

        List<Wallet> lastDays = wallet.subList(wallet.size() - howManyDays, wallet.size());

        List<Integer> date = new ArrayList<>();
        List<String> cash = new ArrayList<>();

        Integer count = 1;
        for (Wallet wallet1 : lastDays) {
            if (date.size() >= howManyDays) {
                break;
            }
            date.add(count);
            cash.add(wallet1.getCashInWallet().toString());
            count++;
        }

        CountingTime countingTime = new CountingTime();
        countingTime.setCountingTimeToChartMyWallet(howManyDays);

        Integer time = countingTime.getCountingTimeToChartMyWallet();

        model.addAttribute("howmany",time);
        model.addAttribute("timetoshow", new CountingTime());
        model.addAttribute("date", date);
        model.addAttribute("cash", cash);
    }

}
