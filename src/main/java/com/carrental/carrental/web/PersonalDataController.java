package com.carrental.carrental.web;

import com.carrental.carrental.domain.*;
import com.carrental.carrental.persistance.UserRepository;
import com.carrental.carrental.service.LoggedInUserService;
import com.uploadcare.api.Client;
import com.uploadcare.api.File;
import com.uploadcare.upload.FileUploader;
import com.uploadcare.upload.UploadFailureException;
import com.uploadcare.upload.Uploader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

@Controller
//@RequestMapping(value = "client/personalinfo")

public class PersonalDataController {

    @Autowired
    private UserRepository userRepository;

    // TODO check it later
    //private Client client = new Client("public", "priv");

    @PostConstruct
    public void initializer() {
    }

    @GetMapping(value = "client/personalinfo/clientpersonalinfo")
    public String clientPersonalInfo(Model model) {
        //TODO temp

//        //Client client = Client.demoClient();
//        java.io.File sourceFile = new java.io.File("d:/publicimg.png");
//        Uploader uploader = new FileUploader(client, sourceFile);
////        try {
////            // send file to cdn
////            File file = uploader.upload().save();
////            System.out.println(file.getOriginalFileUrl());
////        } catch (UploadFailureException e) {
////            System.out.println("Upload failed :(");
////        }
//
//        // delete file
//        try {
//            File fileToDelete = client.getFile("1aa897e1-7649-4d53-9a8c-ca704d2167da");
//            client.deleteFile("1aa897e1-7649-4d53-9a8c-ca704d2167da");
//            System.out.println("File was deleted.");
//        } catch (Exception e) {
//            System.out.println("File was not deleted.");
//        }
//
//        System.out.println("fotosy -> " + client.getFiles().asList());


        //TODO temp
        String login = LoggedInUserService.getLogin();

        User user = userRepository.findUserByLogin(login);
        model.addAttribute("user", user);

        User updatedUser = new User(user);
        model.addAttribute("updatedUser", updatedUser);

        return "client/personalinfo/clientpersonalinfo";
    }

    @PostMapping(value = "client/personalinfo/clientpersonalinfo")
    public String clientPersonalInfoChangeData(@ModelAttribute User updatedUser, Model model) {
        //TODO : validate updated data
        //TODO : update password
        //TODO : add photo to db
        //TODO : add email verification
        //TODO : add limitation of data changing

        String updatedFirstName = updatedUser.getFirstName();
        String updatedLastName = updatedUser.getLastName();
        String updatedLogin = updatedUser.getLogin();

        String login = LoggedInUserService.getLogin();
        User user = userRepository.findUserByLogin(login);

        user.setLogin(updatedLogin);
        user.setFirstName(updatedFirstName);
        user.setLastName(updatedLastName);

        userRepository.save(user);

        if (!login.equals(user.getLogin())) {
            System.out.println("must log out");
            return "client/personalinfo/clientpersonalinfomustlogout";
        }

        return "redirect:clientpersonalinfo";
    }
}
