package com.carrental.carrental.web;

import com.carrental.carrental.domain.User;
import com.carrental.carrental.domain.*;
import com.carrental.carrental.persistance.*;
import com.carrental.carrental.service.DiscountService;
import com.carrental.carrental.service.InformationService;
import com.carrental.carrental.service.LoggedInUserService;
import com.carrental.carrental.tools.DataConverter;
import com.carrental.carrental.tools.DataValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

@Controller
public class MainController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private InformationService informationService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private DiscountService discountService;

    @Autowired
    private DiscountRepository discountRepository;

    @Autowired
    private UserDiscountRepository userDiscountRepository;

    @PostConstruct
    public void initializer() {
        userRepository.save(new User("admin", bCryptPasswordEncoder.encode("123"), "ROLE_ADMIN", "Temp Admin", "Kowalski"));
        userRepository.save(new User("user", bCryptPasswordEncoder.encode("123"), "ROLE_USER", "Temp user", "Nowak"));
        userRepository.save(new User("tom", bCryptPasswordEncoder.encode("123"), "ROLE_USER", "Tom", "Jonnes"));
        userRepository.save(new User("kate", bCryptPasswordEncoder.encode("123"), "ROLE_USER", "Kate", "Willson"));

        List<Car> carsForTests = new ArrayList<>();

        carsForTests.add(new Car(Models.FIAT, 50.0, Color.WHITE, State.FREE, "https://images.netdirector.co.uk/gforces-auto/image/upload/w_1552,h_873,q_auto,c_fill,f_auto,fl_lossy/auto-client/95ce615aecbab21f82898d0da93bcf16/lounge_uk.png"));
        carsForTests.add(new Car(Models.TOYOTA, 80.0, Color.SILVER, State.BROKEN, "http://img.sm360.ca/ir/w500c/images/newcar/2017/toyota/corolla/ce/sedan/exteriorColors/2017_toyota_corolla_berline_ce_argent-classique-metal_001.png"));
        carsForTests.add(new Car(Models.TOYOTA, 90.0, Color.SILVER, State.FREE, "http://img.sm360.ca/ir/w500c/images/newcar/2017/toyota/corolla/ce/sedan/exteriorColors/2017_toyota_corolla_berline_ce_argent-classique-metal_001.png"));
        carsForTests.add(new Car(Models.TOYOTA, 100.0, Color.SILVER, State.BORROW, "http://img.sm360.ca/ir/w500c/images/newcar/2017/toyota/corolla/ce/sedan/exteriorColors/2017_toyota_corolla_berline_ce_argent-classique-metal_001.png"));
        carsForTests.add(new Car(Models.VOLVO, 110.0, Color.BLACK, State.FREE, "https://carwow-uk-wp-3.imgix.net/XC60-Black-Stone.jpeg?ixlib=rb-1.1.0&fit=crop&w=1600&h=&q=60&cs=tinysrgb&auto=format"));
        carsForTests.add(new Car(Models.VOLVO, 120.0, Color.RED, State.FREE, "https://carwow-uk-wp-3.imgix.net/XC60-Black-Stone.jpeg?ixlib=rb-1.1.0&fit=crop&w=1600&h=&q=60&cs=tinysrgb&auto=format"));
        carsForTests.add(new Car(Models.VOLVO, 130.0, Color.BLACK, State.FREE, "https://carwow-uk-wp-3.imgix.net/XC60-Black-Stone.jpeg?ixlib=rb-1.1.0&fit=crop&w=1600&h=&q=60&cs=tinysrgb&auto=format"));
        carsForTests.add(new Car(Models.AUDI, 140.0, Color.WHITE, State.BORROW, "http://img1.sm360.ca/ir/w600h450/images/newcar/2018/audi/a3-sportback-e-tron/progressiv/hybrid/exteriorColors/11748_cc0640_001_t9.png"));
        carsForTests.add(new Car(Models.AUDI, 150.0, Color.RED, State.FREE, "http://img1.sm360.ca/ir/w600h450/images/newcar/2018/audi/a3-sportback-e-tron/progressiv/hybrid/exteriorColors/11748_cc0640_001_t9.png"));
        carsForTests.add(new Car(Models.AUDI, 200.0, Color.WHITE, State.FREE, "http://img1.sm360.ca/ir/w600h450/images/newcar/2018/audi/a3-sportback-e-tron/progressiv/hybrid/exteriorColors/11748_cc0640_001_t9.png"));
        carsForTests.add(new Car(Models.SYRENKA, 15.0, Color.GOLD, State.FREE, "https://b.allegroimg.com/s400/03864e/b81c28144488879ce7bde18edc4b"));
        carsForTests.add(new Car(Models.SYRENKA, 150.0, Color.BLACK, State.FREE, "https://galeria.bankier.pl/p/2/9/dda7f3e6b44084-645-322-0-408-3081-1540.jpg"));
        carsForTests.add(new Car(Models.SYRENKA, 200.0, Color.GOLD, State.FREE, "https://b.allegroimg.com/s400/03864e/b81c28144488879ce7bde18edc4b"));

        carRepository.saveAll(carsForTests);

        Wallet wallet = new Wallet();
        wallet.setCashInWallet(800.0); //cash in start
        wallet.setTransactionDate(LocalDateTime.of(2019,04,02,02,02,20)); //casxh in start

        walletRepository.save(wallet);

        Wallet wallet1 = new Wallet();
        wallet1.setCashInWallet(100.0); //cash in start
        wallet1.setTransactionDate(LocalDateTime.of(2019,04,01,01,01,01)); //casxh in start

        walletRepository.save(wallet1);

        Wallet wallet2 = new Wallet();
        wallet2.setCashInWallet(2000.0); //cash in start
        wallet2.setTransactionDate(LocalDateTime.of(2019,04,03,10,10,10)); //casxh in start

        walletRepository.save(wallet2);

        Wallet wallet3 = new Wallet();
        wallet3.setCashInWallet(1600.0); //cash in start
        wallet3.setTransactionDate(LocalDateTime.of(2019,04,04,10,10,10)); //casxh in start

        walletRepository.save(wallet3);

        Wallet wallet4 = new Wallet();
        wallet4.setCashInWallet(2400.0); //cash in start
        wallet4.setTransactionDate(LocalDateTime.of(2019,04,05,10,10,10)); //casxh in start

        walletRepository.save(wallet4);

        // add discount to user
        discountRepository.saveAll(discountService.getDiscounts());
        Discount discountA = discountRepository.findByName("+10% discount after 1st car");
        Discount discountB = discountRepository.findByName("+5% discount after 3rd car");
        Discount discountC = discountRepository.findByName("+5% discount after 5th car");
        Discount discountD = discountRepository.findByName("-50PLN discount after 1st car");

        List<User> usersA = new ArrayList<>();
        usersA.add(userRepository.findUserByLogin("user"));
        usersA.add(userRepository.findUserByLogin("tom"));

        List<User> usersB = new ArrayList<>();
        usersB.add(userRepository.findUserByLogin("kate"));
        usersB.add(userRepository.findUserByLogin("user"));

        UserDiscount userDiscount1 = new UserDiscount(usersA, discountA);
        userDiscountRepository.save(userDiscount1);

        UserDiscount userDiscount2 = new UserDiscount(usersB, discountB);
        userDiscountRepository.save(userDiscount2);

        UserDiscount userDiscount3 = new UserDiscount(usersB, discountC);
        userDiscountRepository.save(userDiscount3);

        UserDiscount userDiscount4 = new UserDiscount(usersB, discountD);
        userDiscountRepository.save(userDiscount4);
    }

    @GetMapping(value = "/")
    public String home() {
        return "home";
    }

    @GetMapping(value = "/usercheck")
    public String usercheck() {

        String webPage = "/login";

        String role = LoggedInUserService.getRole();

        if (role.equals("ROLE_USER")) {
            webPage = "redirect:client/clientstart";
        } else if (role.equals("ROLE_ADMIN")) {
            webPage = "redirect:admin/adminstart";
        }

        return webPage;
    }

    @GetMapping(value = "/login")
    public String login(@RequestParam(value = "error", required = false) String error, Model model) {
        if (error != null) {
            model.addAttribute("error", true);
            return "wronglogin";
        }
        return null;
    }

    @GetMapping(value = "/createuser")
    private String createUserGet(Model model) {
        model.addAttribute("user", new User());
        return "createuser";
    }

    @PostMapping(value = "/createuser")
    private String createUserPost(@ModelAttribute User user, Model model) {
        user.setAmountOfCarsRentedSoFar(0);

        if (user.getFirstName().isEmpty()
                || user.getLastName().isEmpty()
                || user.getPassword().isEmpty()
                || user.getLogin().isEmpty()) {
            return "createuserfailureempty";
        }

        String firstName = DataConverter.getStringWithFirstLetterCapitalized(user.getFirstName());
        model.addAttribute("firstName", firstName);

        String wrongFirstName = "First name: is correct.";
        boolean dataIsCorrect = true;

        if (DataValidator.isWrongFirstName(firstName)) {
            dataIsCorrect = false;
            wrongFirstName = firstName + " : " + DataValidator.getFirstNameErrorMessage();
        }

        String lastName = DataConverter.getStringWithFirstLetterCapitalized(user.getLastName());
        String wrongLastName = "Last name: is correct.";
        if (DataValidator.isWrongLastName(lastName)) {
            dataIsCorrect = false;
            wrongLastName = lastName + " : " + DataValidator.getLastNameErrorMessage();
        }

        String login = user.getLogin().toLowerCase();
        model.addAttribute("login", login);
        String wrongLogin = "Login : is correct.";
        if (DataValidator.isWrongLogin(login)) {
            dataIsCorrect = false;
            wrongLogin = login + " : " + DataValidator.getLoginErrorMessage();
        }

        String password = user.getPassword();
        model.addAttribute("password", password);
        String wrongPassword = "Password : is correct.";
        if (DataValidator.isWrongPassword(password)) {
            dataIsCorrect = false;
            wrongPassword = password + " : " + DataValidator.getPasswordErrorMessage();
        }

        if (dataIsCorrect) {
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setLogin(login);
            user.setRole("ROLE_USER");
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userRepository.save(user);

            return "createusersuccess";
        } else {
            model.addAttribute("wrongFirstName", wrongFirstName);
            model.addAttribute("wrongLastName", wrongLastName);
            model.addAttribute("wrongLogin", wrongLogin);
            model.addAttribute("wrongPassword", wrongPassword);
            return "createuserfailure";
        }
    }
}
