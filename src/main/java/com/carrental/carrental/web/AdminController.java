package com.carrental.carrental.web;

import com.carrental.carrental.domain.*;
import com.carrental.carrental.persistance.*;
import com.carrental.carrental.service.AdminService;
import com.carrental.carrental.service.InformationService;
import com.carrental.carrental.service.LoggedInUserService;
import com.carrental.carrental.service.WalletService;
import com.carrental.carrental.tools.CountingTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping(value = "admin")
public class AdminController {

    private final Double REPAIR_CAR = -50.0;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private AdminService adminService;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private WalletService walletService;

    @Autowired
    private UserCarRepository userCarRepository;

    @Autowired
    private CarBorrowingRepository carBorrowingRepository;

    @Autowired
    private CarRepairingRepository carRepairingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WalletRepository walletRepository;

    private CountingTime countingTime;

    @Autowired
    private InformationService informationService;


    private List<Color> colorsList;

    private List<Models> modelsList;

    private List<State> statesList;

    @PostConstruct
    public void initializer() {
        colorsList = Arrays.asList(Color.values());
        modelsList = Arrays.asList(Models.values());
        statesList = Arrays.asList(State.values());
    }

    @GetMapping(value = "add")
    public String prepareFormToAddCar(Model model) {

        colorsList = Arrays.asList(Color.values());

        model.addAttribute("models", Arrays.asList(Models.values()));
        model.addAttribute("colors", colorsList);
        model.addAttribute("car", new Car());
        return "admin/add";
    }

    @GetMapping(value = "addcar")
    public String addCar(@ModelAttribute Car car, Model model) {

        if (car.getModel().equals(Models.NONE) || car.getColor().equals(Color.NONE)) {
            return "admin/createcarnonechossen";
        }
        if (car.getPrice().isNaN()) {
            return "admin/createcarpriceproblem";
        }

        if (car.getUrlPhoto().length() != 0) {
            informationService.setUrlPhoto(car.getUrlPhoto());
        } else {
            informationService.setUrlPhoto("http://www.appledystopia.com/wp-content/uploads/2015/02/no-apple-car.png");
        }

        informationService.setModel(car.getModel());
        informationService.setPrice(car.getPrice());
        informationService.setColor(car.getColor());

        model.addAttribute("model", car.getModel());
        model.addAttribute("price", car.getPrice());
        model.addAttribute("color", car.getColor());
        model.addAttribute("url", informationService.getUrlPhoto());

        return "admin/confirmaddcar";
    }

    @GetMapping(value = "carindb")
    public String carindb(@ModelAttribute Car car) {

        car.setModel(informationService.getModel());
        car.setPrice(informationService.getPrice());
        car.setColor(informationService.getColor());
        car.setState(State.FREE);
        car.setUrlPhoto(informationService.getUrlPhoto());
        carRepository.save(car);
        return "admin/addcar";
    }

    @GetMapping(value = "adminCar/{chosenCarId}")
    public String adminCarInformation(@PathVariable("chosenCarId") String carId, Model model) {

        informationService.setCarId(Long.valueOf(carId));

        Long currentCarId = informationService.getCarId();
        Car car = carRepository.findById(currentCarId).get();

        UserCar userCar = userCarRepository.findByCar(car);
        CarBorrowing carBorrowing = carBorrowingRepository.findByUserCar(userCar);

        if (userCar != null) {
            model.addAttribute("userFirstName", userCar.getUser().getFirstName());
            model.addAttribute("userLastName", userCar.getUser().getLastName());
            model.addAttribute("userEnterDate", carBorrowing.getDateEnter());
            model.addAttribute("userExitDate", carBorrowing.getDateExit());
            model.addAttribute("userTravelFrom", carBorrowing.getCityEnter());
            model.addAttribute("userTravelTo", carBorrowing.getCityExit());
            model.addAttribute("priceAll", carBorrowing.getPriceForAllDay());

//            System.out.println("Jest!");
        } else {
            model.addAttribute("userFirstName", "----");
            model.addAttribute("userLastName", "----");
            model.addAttribute("userEnterDate", "----");
            model.addAttribute("userExitDate", "----");
            model.addAttribute("userTravelFrom", "----");
            model.addAttribute("userTravelTo", "----");
            model.addAttribute("priceAll", "----");

        }

        // System.out.println("CarId is: " + car.getIdCar());

        model.addAttribute("models", car.getModel());
        model.addAttribute("colors", car.getColor());
        model.addAttribute("idCar", car.getIdCar());
        model.addAttribute("price", car.getPrice());
        model.addAttribute("state", car.getState());
        model.addAttribute("url", car.getUrlPhoto());
        model.addAttribute("myWallet", walletService.prepareNewWallet(0.0));

        return "admin/admincarinfo";
    }

    @GetMapping(value = "adminstart")
    public String adminStart(Model model) {
        String login = LoggedInUserService.getLogin();
        model.addAttribute("login", login);
        return "admin/adminstart";
    }

    @GetMapping(value = "car")
    public String adminCar(Model model) {
        Iterable<Car> cars = carRepository.findAll();
        getString(adminService, model, cars);
        return "admin/car";
    }

    @GetMapping(value = "users")
    public String adminUser(Model model) {

        List<User> users = userRepository.findAll();
        model.addAttribute("users", users);
        return "admin/users";
    }

    @PostMapping(value = "repair")
    public String repairCar(Model model) {

        Long idCar = informationService.getCarId();
        Car car = carRepository.readCarByIdCar(idCar);

        if (car.getState() == State.BROKEN) {
            car.setState(State.FREE);
            carRepository.save(car);
        }

        String login = LoggedInUserService.getLogin();
        User user = userRepository.findUserByLogin(login);

        carRepository.save(car);

        Iterable<Car> cars = carRepository.findAll();

        CarRepairing carRepairing = new CarRepairing();
        carRepairing.setCar(car);
        carRepairing.setUser(user);
        carRepairing.setLocalDateTime(LocalDateTime.now());
        carRepairingRepository.save(carRepairing);

        walletService.addMoney(REPAIR_CAR, carRepairing);

        getString(adminService, model, cars);

        return "admin/car";
    }

    @GetMapping(value = "cash")
    public String adminCash(Model model) {

        Iterable<Wallet> wallets = walletRepository.findAll();

        User fakeUser = new User("none", "none", "none", "none", "none");
        fakeUser.setIdUser(1L);
        Car fakeCar = new Car(Models.NONE, 0.0, Color.NONE, State.NONE, "");
        fakeCar.setIdCar(1L);
        UserCar fakeUserCar = new UserCar(fakeUser, fakeCar);
        fakeUser.setIdUser(1L);
        CarBorrowing fakeCarBorrowing =
                new CarBorrowing(fakeUserCar, 0, LocalDate.now(), LocalDate.now(), 0.0, 0.0, City.KRAKÓW, City.KRAKÓW);
        CarRepairing fakeCarRepairing = new CarRepairing(fakeCar, fakeUser, LocalDateTime.now());


        List<Wallet> wallet = new ArrayList<>();
        for (Wallet listWallet : wallets) {
            //wallet.add(listWallet);

            Wallet currentWallet = new Wallet(listWallet);

            if (currentWallet.getCarBorrowing() == null) {
                currentWallet.setCarBorrowing(fakeCarBorrowing);
            }

            if (currentWallet.getCarRepairing() == null) {
                currentWallet.setCarRepairing(fakeCarRepairing);
            }

            wallet.add(currentWallet);
        }

        Collections.sort(wallet);

        model.addAttribute("wallet", wallet);

        return "admin/cash";
    }

    @GetMapping(value = "cashinchart")
    public String cashInChar(Model model) {

        Integer howManyDays = 10;

        walletService.cashFilter(howManyDays,model);

        return "admin/cashchart";
    }

    @GetMapping(value = "chartfilter")
    public String cashFilter(@ModelAttribute CountingTime countingTime, Model model){

        Integer howManyDays = countingTime.getCountingTimeToChartMyWallet();

        walletService.cashFilter(howManyDays,model);

        return "admin/cashchart";

    }

    @PostMapping(value = "adminCarFilter")
    public String adminCarFilter(@ModelAttribute AdminService adminService, Model model) {

        informationService.setModel(adminService.getModels());
        Models modelFilter = informationService.getModel();
        informationService.setColor(adminService.getColor());
        Color colorFilter = informationService.getColor();
        informationService.setState(adminService.getState());
        State stateFilter = informationService.getState(); //TODO refactoring?? 3 line to delete... maybe not put information to informationService...

        Iterable<Car> cars = getFilter(modelFilter, colorFilter, stateFilter);

        getString(adminService, model, cars);

        return "admin/car";
    }



    // duplicate code
    private Model getString(@ModelAttribute AdminService adminService, Model model, Iterable<Car> cars) {

        model.addAttribute("cars", cars);
        model.addAttribute("models", modelsList);
        model.addAttribute("model", Models.values());
        model.addAttribute("colors", colorsList);
        model.addAttribute("color", Color.values());
        model.addAttribute("states", statesList);
        model.addAttribute("state", State.values());
        model.addAttribute("adminService", adminService);

        return model;
    }

    // filter of Cars
    private Iterable<Car> getFilter(Models modelFilter, Color colorFilter, State stateFilter) {

        Iterable<Car> cars = carRepository.findAll();

        if (modelFilter != Models.NONE) {
            if (colorFilter != Color.NONE) {
                if (stateFilter != State.NONE) {
                    cars = carRepository.findByModelAndColorAndState(modelFilter, colorFilter, stateFilter);
                } else {
                    cars = carRepository.findByModelAndColor(modelFilter, colorFilter);
                }
            } else if (stateFilter != State.NONE) {
                cars = carRepository.findByModelAndState(modelFilter, stateFilter);
            } else {
                cars = carRepository.findByModel(modelFilter);
            }

        } else if (colorFilter != Color.NONE) {
            if (stateFilter != State.NONE) {
                cars = carRepository.findByColorAndState(colorFilter, stateFilter);
            } else {
                cars = carRepository.findByColor(colorFilter);
            }
        } else if (stateFilter != State.NONE) {
            cars = carRepository.findByState(stateFilter);
        }
        return cars;
    }


}
