package com.carrental.carrental.domain;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Getter
@Setter
public class Wallet implements Comparable<Wallet> {

    @Id
    @GeneratedValue
    private Long idWallet;

    private Double cashInWallet;

    private LocalDateTime transactionDate;

    @OneToOne
    private CarBorrowing carBorrowing;

    @OneToOne
    private CarRepairing carRepairing;

    public Wallet() {
    }

    public Wallet(Wallet otherWallet) {
        this.cashInWallet = otherWallet.cashInWallet;
        this.transactionDate = otherWallet.transactionDate;
        this.carBorrowing = otherWallet.carBorrowing;
        this.carRepairing = otherWallet.carRepairing;
    }

    public Double addMoney(Double money) {
        return cashInWallet += money;
    }

    @Override
    public int compareTo(@NotNull Wallet w) {
        if (this.transactionDate.isBefore(w.transactionDate)) {
            return -1;
        } else if (this.transactionDate.isAfter(w.transactionDate)) {
            return 1;
        } else return 0;

    }

    @Override
    public String toString() {
        return "Wallet{" +
                "idWallet=" + idWallet +
                ", cashInWallet=" + cashInWallet +
                ", transactionDate=" + transactionDate +
                ", carBorrowing=" + carBorrowing +
                ", carRepairing=" + carRepairing +
                '}';
    }

    public String dateToString() {
        return transactionDate.getYear() + " - " +transactionDate.getMonthValue() + " - " + transactionDate.getDayOfMonth();
    }
}