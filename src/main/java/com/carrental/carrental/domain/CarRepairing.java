package com.carrental.carrental.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class CarRepairing {

    @Id
    @GeneratedValue
    private Long idCarRepairing;

    @OneToOne
    private Car car;

    @OneToOne
    private User user;

    private LocalDateTime localDateTime;

    public CarRepairing() {
    }

    public CarRepairing(Car car, User user, LocalDateTime localDateTime) {
        this.car = car;
        this.user = user;
        this.localDateTime = localDateTime;
    }
}
