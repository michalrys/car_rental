package com.carrental.carrental.domain;

public enum Color {
    NONE,
    BLACK,
    WHITE,
    GREEN,
    RED,
    YELLOW,
    GOLD,
    SILVER,
    PURPLE,

}
