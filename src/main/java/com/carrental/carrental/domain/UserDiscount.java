package com.carrental.carrental.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class UserDiscount {
    @Id
    @GeneratedValue
    private Long id;

    // becuase of error: https://stackoverflow.com/questions/11746499/how-to-solve-the-failed-to-lazily-initialize-a-collection-of-role-hibernate-ex
    // error: failed to lazily initialize a collection
    @ManyToMany(fetch = FetchType.EAGER)
    private List<User> users;

    @OneToOne
    private Discount discount;

    public UserDiscount() {
    }

    public UserDiscount(List<User> users, Discount discount) {
        this.users = users;
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "UserDiscount{" +
                "users=" + users +
                ", discount=" + discount +
                '}';
    }
}