package com.carrental.carrental.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Setter
@Getter
public class User implements Serializable {
    @Id
    @GeneratedValue
    private Long idUser;
    @Column
    private String login;
    @Column
    private String password;
    @Column
    private String role;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private Integer amountOfCarsRentedSoFar;

    public User() {
    }

    public User(String login, String password, String role, String firstName, String lastName) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        amountOfCarsRentedSoFar = 0;
    }
    // this is redundant
    public User(User user) {
        this.login = user.getLogin();
        this.password = user.getPassword();
        this.role = user.getRole();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.amountOfCarsRentedSoFar = user.amountOfCarsRentedSoFar;
    }

    public void increaseAmountOfCarsRentedSoFar () {
        amountOfCarsRentedSoFar++;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", role='" + role + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", amountOfCarsRentedSoFar=" + amountOfCarsRentedSoFar +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(idUser, user.idUser) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(role, user.role) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(amountOfCarsRentedSoFar, user.amountOfCarsRentedSoFar);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUser, login, password, role, firstName, lastName, amountOfCarsRentedSoFar);
    }
}