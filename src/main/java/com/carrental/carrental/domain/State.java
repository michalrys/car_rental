package com.carrental.carrental.domain;

public enum State {
    NONE,
    FREE,
    BORROW,
    BROKEN
}
