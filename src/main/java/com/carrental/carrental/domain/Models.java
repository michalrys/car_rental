package com.carrental.carrental.domain;

public enum Models {
    NONE,
    TOYOTA,
    FIAT,
    VOLVO,
    AUDI,
    SYRENKA
}
