package com.carrental.carrental.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class UserCar {

    @Id
    @GeneratedValue
    private Long idUserCar;

    @OneToOne
    private User user;

    @OneToOne
    private Car car;

    public UserCar(User user, Car car) {
        this.user = user;
        this.car = car;
    }

    public UserCar() {
    }

}
