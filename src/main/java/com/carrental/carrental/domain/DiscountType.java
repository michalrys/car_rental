package com.carrental.carrental.domain;

public enum DiscountType {
    PERCENTAGE,
    REAL;
}
