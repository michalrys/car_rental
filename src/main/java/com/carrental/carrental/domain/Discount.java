package com.carrental.carrental.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Discount {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private String rulesDescription;

    @Column
    private Double valuePercentage;

    @Column
    private Double valueReal;

    @Column
    private Double startingPoint;

    public Discount() {
    }

    public Discount(String name, String rulesDescription, Double valuePercentage, Double valueReal, Double startingPoint) {
        this.name = name;
        this.rulesDescription = rulesDescription;
        this.valuePercentage = valuePercentage;
        this.valueReal = valueReal;
        this.startingPoint = startingPoint;
    }

    @Override
    public String toString() {
        return "Discount{" +
                "name='" + name + '\'' +
                ", rulesDescription='" + rulesDescription + '\'' +
                ", valuePercentage=" + valuePercentage +
                ", valueReal=" + valueReal +
                ", startingPoint=" + startingPoint +
                '}';
    }
}
