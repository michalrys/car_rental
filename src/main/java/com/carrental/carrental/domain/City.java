package com.carrental.carrental.domain;

public enum City {
    KATOWICE,
    KRAKÓW,
    WARSZAWA,
    GDAŃSK,
    POZNAŃ
}
